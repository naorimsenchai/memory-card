VANTA.FOG({
	el: "#vanta",
	mouseControls: true,
	touchControls: true,
	gyroControls: false,
	minHeight: 200.00,
	minWidth: 200.00,
	highlightColor: 0xa300ff,
	midtoneColor: 0x0,
	lowlightColor: 0x8900ff,
	baseColor: 0x0,
	blurFactor: 0.70,
	speed: 2.00
});